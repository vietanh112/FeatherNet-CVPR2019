import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from sklearn.metrics import roc_curve, roc_auc_score


def cal_metric(target, predicted,show = False):
    fpr, tpr, thresholds = roc_curve(target, predicted)
    _tpr = (tpr)
    _fpr = (fpr)
    tpr = tpr.reshape((tpr.shape[0],1))
    fpr = fpr.reshape((fpr.shape[0],1))
    scale = np.arange(0, 1, 1e-8)
    function = interp1d(_fpr, _tpr)
    y = function(scale)
    znew = abs(scale + y -1)
    eer = scale[np.argmin(znew)]
    FPRs = {"TPR@FPR=10E-2": 1e-2, "TPR@FPR=10E-3": 1e-3, "TPR@FPR=10E-4": 1e-4}
    TPRs = {"TPR@FPR=10E-2": 1e-2, "TPR@FPR=10E-3": 1e-3, "TPR@FPR=10E-4": 1e-4}
    for i, (key, value) in enumerate(FPRs.items()):
        index = np.argwhere(scale == value)
        score = y[index] 
        TPRs[key] = float(np.squeeze(score))
    auc = roc_auc_score(target, predicted)
    if show:
        plt.plot(scale, y)
        plt.show()
    return eer,TPRs, auc,{'x':scale, 'y':y}
